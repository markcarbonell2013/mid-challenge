#! /usr/bin/env bash

SCRIPT_DIR="$(cd "$(dirname "$0")" > /dev/null 2>&1 && pwd)"
RANDOM_SEED=1234

cd $SCRIPT_DIR/../rasabot

echo "Splitting nlu data..."
rasa data split nlu -u data/ \
          --training-fraction 0.75 \
          --random-seed "$RANDOM_SEED" \
          --out test_train_split/

cd -




