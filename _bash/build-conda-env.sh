#! /usr/bin/env bash

SCRIPT_DIR="$(cd "$(dirname "$0")" > /dev/null 2>&1 && pwd)" 

cd $SCRIPT_DIR/../

~/anaconda3/envs/mid/bin/pip install --upgrade pip
~/anaconda3/envs/mid/bin/pip install -r requirements.txt

cd -
