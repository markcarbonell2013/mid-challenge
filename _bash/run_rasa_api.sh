#! /usr/bin/env bash

SCRIPT_DIR="$(cd "$(dirname "$0")" > /dev/null 2>&1 && pwd)"
MODEL_NAME="main-model-v1.0"

cd $SCRIPT_DIR/../rasabot

rasa train nlu --fixed-model-name "$MODEL_NAME" \
          --persist-nlu-data \
          --num-threads 4 \
          -d domain.yml \
          -c config.yml \
          -u test_train_split/training_data.yml

cd -




