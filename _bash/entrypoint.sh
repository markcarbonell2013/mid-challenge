#! /usr/bin/env bash

uvicorn main:app --port 8080 --host localhost --reload --proxy-headers --debug

