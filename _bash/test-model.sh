#! /usr/bin/env bash

SCRIPT_DIR="$(cd "$(dirname "$0")" > /dev/null 2>&1 && pwd)"
MODEL_NAME="main-model-v1.tar.gz"

cd $SCRIPT_DIR/../rasabot

rasa test nlu -m "models/$MODEL_NAME" \
          --successes \
          --out results/ \
          --config config.yml \
          -r 5 \
          -u test_train_split/test_data.yml

cd -




