
# MID Challenge

This document contains the points described by the requirements in [this project's wiki](https://gitlab.com/markcarbonell2013/mid-challenge/-/wikis/home)

# Project Setup

1. Clone this project by running
```bash
git clone https://gitlab.com/markcarbonell2013/mid-challenge
cd mid-challenge
```
2. Once inside, make sure you have `conda` installed in your system, else install it
3. With conda installed, you can load the project's dependencies with
```bash
conda env create --file environment.yml
```
4. Then activate the conda environment in your console with
```bash
conda activate mid
```
5. Start the api by running inside of one console
```bash
cd mid-challenge
sh _bash/entrypoint.sh
```

6. Then you can test the API if you want by sending  messages to it.
To test the API I wrote the file `tests/test_api.py`. To use it run
```bash
python3 tests/test_api.py
```
It will show you the output produced by the model from the default message:
```python
{
    "message_uuid": "f6233bff-0b05-41f6-b3a2-e5ebdc0c1383",
    "message": "Ich würde gerne meine Stammdaten ändern und danach noch Getränke bestellen" 
}
```
If you wish to change that message then modify it directly in `tests/test_api.py` and run the script again. Something like
```
cd tests/
vim test_api.py # edit file with new message
python3 test_api.py
```

You should see an output like the result below:
![screenshot of results](./images/result.png)

# 1. Model domain and training

- The model's domain is an agent that processes requests for a food delivery service. Users are supposed to use it to order drinks, food and change their profile data like name, email, etc.
- The model supports the intents that are defined per default when one runs `rasa init`
- In order to view the training examples used for the model do
```bash
cat rasabot/domain.yml
cat rasabot/data/nlu.yml
```
- Before training the model I performed a 75/25 train-test splits using the utility script
```bash
sh _bash/split-nlu-data.sh
```
- To train the model, I wrote the utlity script
```bash
sh _bash/train-model.sh
```
It stores the finally trained model under `rasabot/models/main-model-v1.tar.gz`

# 2. Model Description

- The model's pipeline uses the supervised embeddings pipeline provided by Rasa and optimizes it to perform multi-intent classification.
- You can check the model's pipeline architecture with
```bash
cat rasabot/config.yml
```
- If you want to know the differences between my file and the default rasa pipeline you can make a diff of the default rasa file against `rasabot/config.yml`.

## Architecture justification
- The supervised embeddings pipeline weights are untrained, this makes it easier to adapt in a specific domain in the long term. 
- The supervised embeddings pipeline can be easily modified to perform MID classification tasks by modifying the `rasabot/config.yml` file.
- I am aware this pipeline requires much more training data to be used in production, I wanted to train in on the `de_core_news_md` dataset provided by Spacy, but I did not due to time constraints.

# Model evaluation

- An evaluation of the model can be seen by running
```bash
cat rasabot/results/intent_report.json
```
It contains the accuracy results as well as the F1 metric results for each intent.
- The model can be evaluated using the utility script
```bash
sh _bash/test-model.sh
```

# Other considerations

I would have loved to further extend the domain of this model to improve it's accuracy if I had more time.
The main challenge I worked on the [devops_challenge](https://gitlab.com/markcarbonell2013/devops_challenge). You can view it in Gitlab too.







