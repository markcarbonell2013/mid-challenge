import uuid

def validate_uuid_version(uuid_hex_str : str, version: int) -> bool:
    try:
        val = uuid.UUID(uuid_hex_str, version=version)
    except ValueError:
        return False
    return val.hex == uuid_hex_str

def validate_response_dict(response_dict: dict) -> bool:
    dict_keys = ["message_uuid", "intents"]
    intent_keys = ["meta", "confidence", "intent"]
    if not sorted(response_dict.keys()) == sorted(dict_keys):
        return False

    for intent in response_dict["intents"]:
        if not sorted(intent.keys()) == sorted(intent_keys):
            return False
        if not isinstance(intent["confidence"], float):
            return False
        if not isinstance(intent["intent"], str):
            return False
        if not isinstance(intent["meta"], dict):
            return False

    return True




