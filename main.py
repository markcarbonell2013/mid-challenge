import uuid
from fastapi import FastAPI
from entities.utterance import Utterance
from entities.utterance_response import UtteranceResponse
from utils.utility_functions import validate_uuid_version
from controllers.utterance_handler import utterance_handler

app = FastAPI()

@app.post("/resolve_intents_from_user_utterance", response_model=UtteranceResponse)
async def post_resolve_intents_from_user_utterance(utterance: Utterance):
    intents = await utterance_handler(utterance.message)
    return {
            "message_uuid": utterance.message_uuid,
            "intents": intents
    }


