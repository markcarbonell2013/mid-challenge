import uuid
from pydantic import BaseModel

class Utterance(BaseModel):
    message_uuid: uuid.UUID
    message: str

    class Config:
        orm_mode = True

