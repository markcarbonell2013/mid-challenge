import uuid
from pydantic import BaseModel

class UtteranceResponse(BaseModel):
    message_uuid: uuid.UUID
    intents: list

    class Config:
        orm_mode = True

