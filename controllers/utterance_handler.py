import sys
import os
current_dir = os.path.abspath(os.path.dirname(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)
from agent import agent

async def get_interpretation(message):
    interpretation = await agent.parse_message(message)
    return interpretation

def build_dict_response_from_intents(intents_arr):
    for intent_dict in intents_arr:
        intent_dict["intent"] = intent_dict.pop("name")
        intent_dict["meta"] = {}
    return intents_arr


async def utterance_handler(message):
    interpretation = await get_interpretation(message)
    intent_ranking = interpretation["intent_ranking"]
    intent_ranking.sort(key=lambda x: x["confidence"], reverse=True)
    best_three_intents = intent_ranking[:3]
    intent_response = build_dict_response_from_intents(best_three_intents)
    return intent_response

