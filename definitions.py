import os
from dotenv import load_dotenv

load_dotenv()

EXTERNAL_HOST = os.getenv("EXTERNAL_HOST")
EXTERNAL_PORT = os.getenv("EXTERNAL_PORT")
INTERNAL_HOST = os.getenv("INTERNAL_HOST")
INTERNAL_PORT = os.getenv("INTERNAL_PORT")
RASA_PORT = os.getenv("INTERNAL_PORT")

root_dir = os.path.dirname(os.path.abspath(__file__))
base_url = f"http://{INTERNAL_HOST}:{INTERNAL_PORT}"

