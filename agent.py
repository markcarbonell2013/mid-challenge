import os
from rasa.core.agent import Agent
from definitions import root_dir

model_name = "main-model-v1.tar.gz"
model_path = os.path.join(root_dir, "rasabot", "models", model_name)
agent = Agent.load(model_path=model_path)
