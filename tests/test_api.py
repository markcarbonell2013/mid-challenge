import sys
import os
current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(current_dir, '..')
sys.path.append(parent_dir)
import requests
import json
import unittest
import uuid

from definitions import base_url
from utils.utility_functions import validate_response_dict

class TestAPI(unittest.TestCase):
    def setUp(self):
        self.base_url = base_url
        self.route = f"{self.base_url}/resolve_intents_from_user_utterance"
        self.test_message = {}
        self.headers = {"content-type": "application/json"}

    def test_succesful_message(self):
        self.test_message = {
            "message_uuid" : "f6233bff-0b05-41f6-b3a2-e5ebdc0c1383",
            "message": "Ich würde gerne meine Stammdaten ändern und danach noch Getränke bestellen" 
        }

        response = requests.post(self.route, json = self.test_message, headers=self.headers)
        response = json.loads(response.text)
        print(response)
        assert validate_response_dict(response), f"The response should be an intent array but it is {reponse}"

if __name__ == '__main__':
    unittest.main()
